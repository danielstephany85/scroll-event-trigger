
(function(){

    /* check for passive scroll suport ******************************************/
    var passiveSupported = false;
    
    try {
      var options = Object.defineProperty({}, "passive", {
        get: function() {
          passiveSupported = true;
        }
      });
      window.addEventListener("test", null, options);
    } catch(err) {}
    
    function ScrollTrigger(){
        this.scroll_items = document.querySelectorAll("[data-scroll-trigger]");
        this.element_data = [];
        this.last_scroll_pos = 0;
        this.window_data = {};

        return this.init();
    }
    
    ScrollTrigger.prototype.init = function(){
        var self = this;
        this.setElementData();
        this.setWindowData();
        this.setEvents();
        this.resizeThrottled(function(){
            self.setWindowData();
            self.setElementData();
        },function(){
            self.setWindowData();
            self.setElementData();
        });
    }

    ScrollTrigger.prototype.setWindowData = function(){
        this.window_data = {};
        this.last_scroll_pos = window.scrollY;
        this.window_data._innerWidth = window.innerWidth;
        this.window_data._innerHeight = window.innerHeight;
    }

    ScrollTrigger.prototype.setElementData = function(){
        var self = this;
        var data = {};
        this.element_data = [];

        this.scroll_items.forEach(function(element, i) { 
            data._index = i;
            data._repeat = true;
            data._element = element;
            data._clientHeight = element.clientHeight;
            data._offsetTop = element.offsetTop;
            data._element_base = element.offsetTop + element.clientHeight;
            data._triggerOffset = self.parseOffset(element);
            console.log(data._triggerOffset);
            data._scrollRepeat = self.parseScrollRepeat(element);
            data._triggerClass = element.dataset.scrollTrigger;
            data._triggered = false;
            data._down_trigger = data._offsetTop + data._triggerOffset;
            data._up_trigger = data._element_base - data._triggerOffset;
            self.element_data.push(data);
            data = {}
        });
        // console.dir(self.element_data);
    }

    // determinis is the offset value is in pixels or percent and returns value
    ScrollTrigger.prototype.parseOffset = function(element){
        if(element.dataset.triggerOffset){
            var offset_value;
            var offset = element.dataset.triggerOffset;
            // if pixle value
            if(element.dataset.triggerOffset.slice((offset.length-2), offset.length).toLowerCase() == "px" ){
                offset_value = element.dataset.triggerOffset.slice(0, (offset.length-2));
            // if precent value
            } else if(element.dataset.triggerOffset.slice((offset.length-1), offset.length).toLowerCase() == "%" ){
                offset_value = element.dataset.triggerOffset.slice(0, (offset.length-1));
            // if no value type is given defaults to pixle
            }else {
                offset_value = element.dataset.triggerOffset;
            }
            //convert string value to int
            offset_value = parseInt(offset_value);
            // if not a number default to 0
            if(!isNaN(offset_value)){
                return offset_value; 
            }
            return 0;
        } else {
            return 0;
        }
    }


    // determinis is the offset value is in pixels or percent and returns value
    ScrollTrigger.prototype.parseScrollRepeat = function(element){
        if(element.dataset.scrollRepeat == ''){
            return true;
        }else {
            return false;
        }
    }


    ScrollTrigger.prototype.setEvents = function(){
        var self = this;
        var ticking = false;
        self.last_scroll_pos = window.scrollY
        window.addEventListener('scroll', function(){
              if (!ticking) {
                setTimeout(function() {
                    window.requestAnimationFrame(function(){
                        self.scrollEvent(self)
                    });
                  ticking = false;
                }, 25);
                ticking = true;
              }
        }, passiveSupported ? { passive: true } : false)
    }



    ScrollTrigger.prototype.scrollEvent = function(self){
        self._current_scroll_pos = window.scrollY;
        self._window_bottom = self._current_scroll_pos + self.window_data._innerHeight;

        self.element_data.forEach(function(element, i) { 
            //if scrolling down
            if(self._current_scroll_pos > self.last_scroll_pos && element._repeat){
                //if element has entered the view and reached its offset
                if(self._window_bottom > element._down_trigger && self._current_scroll_pos < element._element_base && !element._triggered){
                    console.log("set");
                    element._element.classList.add(element._triggerClass); 
                    self.element_data[i]._triggered = true;
                    if(!element._scrollRepeat){ 
                        element._repeat = false;
                    }
                    //if element has left the view
                } else if(self._current_scroll_pos > element._element_base && element._triggered){
                    console.log("unset");
                    element._element.classList.remove(element._triggerClass); 
                    self.element_data[i]._triggered = false;
                    if(!element._scrollRepeat){
                        element._repeat = false;
                    }
                }
            }
            //if scrolling up
            if(self._current_scroll_pos < self.last_scroll_pos && element._repeat){
                //if element has entered the view and reached its offset
                if(self._current_scroll_pos < element._up_trigger && self._window_bottom > element._offsetTop && !element._triggered){
                    console.log("set2");
                    self.element_data[i]._triggered = true;
                    element._element.classList.add(element._triggerClass);
                    if(!element._scrollRepeat){
                        element._repeat = false;
                    }
                }
                //if element has left the view
                else if(self._window_bottom < element._offsetTop && element._triggered){
                    console.log("unset2");
                    self.element_data[i]._triggered = false;
                    element._element.classList.remove(element._triggerClass);
                    if(!element._scrollRepeat){
                        element._repeat = false;
                    }
                }
            }
        });
        self.last_scroll_pos = window.scrollY;
    }


    ScrollTrigger.prototype.resizeThrottled = function(startcb, endcb){
        var resizing = false;
        var resizeTime;
        var resizeArgs = arguments.length;
        window.addEventListener("resize", function(){
            resizeTime = new Date().getTime();
            if(!resizing){
                resizing = true;
                var resizeCheck = setInterval(function(){
                    var resizeCheckTimer = new Date().getTime();
                    if((resizeTime + 50) < resizeCheckTimer){
                        resizing = false;
                        clearInterval(resizeCheck);
                        if(resizeArgs == 1){
                            return startcb();
                        } else if (resizeArgs == 2) {
                            return endcb();
                        }
                    }
                }, 100);
                if(resizeArgs == 2){
                    return startcb();
                }
            }
        });
    }


    var scrollIt = new ScrollTrigger(); 
})();


// var max_scroll = document.body.offsetHeight - window.innerHeight;

// win.addEventListener('scroll', function(){
//     var scroll_perc = parseFloat(Math.min(window.pageYOffset / max_scroll, 1).toFixed(2));

//     TweenMax.to(tl, 0, {
//         progress: scroll_perc
//     });
// });

// var tl = new TimelineMax({paused: true});